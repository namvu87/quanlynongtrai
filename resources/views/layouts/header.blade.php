<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-top d-flex flex-row justify-content-between align-items-center">
                    <div class="d-none d-sm-block">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <span>Trung Tâm Giống Cây Trồng Học Viện Nông Nghiệp</span>
                    </div>
                    <div>

                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>dangcaygiong123@gmail.com</span>
                        <span class="space">|</span>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        0394.253.666
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner-top-wrap d-flex justify-content-between align-items-center">
                        <div class="item logo d-flex align-items-center">
                            <a href="index.html" class="d-none d-sm-block">
                                <img src="images/web/Logo/logo.png" alt="">
                            </a>
                            <div class="brand-name">
                                <h5>Trung Tâm Giống Cây Trồng Học Viện Nông Nghiệp</h5>
                                <span>Địa chỉ: Học Viện Nông Nghiệp Việt Nam - Trâu Quỳ - Gia Lâm - Hà Nội</span>
                                <h5 class="d-block d-sm-none">Liên hệ: 0394.253.666</h5>
                            </div>
                        </div>
                        <div class="item d-none d-sm-block">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-column brand-name">
                                    <h5>Liên hệ: 0394.253.666</h5>
                                </div>
                            </div>
                        </div>


                        <div class="item d-none d-sm-block">
                            <a href="gio-hang.html" class="btn btn-success">
                                <i class="fa fa-shopping-bag" aria-hidden="true" style="margin-right: 5px"></i>
                                <span style="font-size: 14px">Giỏ hàng / 0 SP</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
