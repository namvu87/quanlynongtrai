<!DOCTYPE html>
<html lang="vi">

<!-- Mirrored from caygiong123.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Apr 2021 05:45:06 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="FaoPnTE5A8OHWmhLRHFlJi7rblrgWfU9marwHV4J">
    <title> Trang chủ
        :: Trung Tâm Giống Cây Trồng - Viện Nông Nghiệp</title>
    <link rel="stylesheet" type="text/css" href="web/librarys/bootstrap-4.1.1/dist/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="css/web/now-ui-kit.css">
    <link rel="stylesheet" type="text/css" href="css/web/style30f4.css?v=3">
    <link rel="stylesheet" type="text/css" href="css/web/style12330f4.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/librarys/customs-1.0.1/css/call7839.css?v=1.2">
    <link rel="stylesheet" type="text/css"
          href="../maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300|Material+Icons'>
    <style type="text/css">
        .abc {
            background-clip: content-box;
            background-color: #fff;
            padding-top: 20px;
        }

        #exampleModalLong {
            z-index: 999999;
        }
    </style>

    <link rel="stylesheet" href="{!! asset('web/librarys/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') !!}">
    <link rel="stylesheet"
          href="{!! asset('web/librarys/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('web/librarys/OwlCarousel2-2.3.4/dist/assets/custom.css') !!}">

</head>
<body>
<div id="main">
    @include('layouts.header')
    <nav>
        <div class="container d-none d-sm-block">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-top d-flex flex-row justify-content-between align-items-center">
                        <div class="item align-items-center">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a class="nav-link active" href="index.html">Trang chủ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="gioi-thieu.html">Giới thiệu</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="thanh-toan.html">Hướng dẫn mua hàng</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="danh-sach-tin-tuc.html">Tin tức thị trường</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="lien-he.html">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                        <div class="item align-items-center">
                            <div class="form-group">
                                <form method="GET" action="http://caygiong123.com/tim-kiem">
                                    <input
                                        value=""
                                        name="q"
                                        type="text"
                                        placeholder="Bạn cần tìm gì ? ..."
                                        class="form-control form-control-success">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container d-block d-sm-none">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-top menu-top-mobile d-flex flex-row justify-content-between align-items-center">
                        <div class="item">
                            <button type="button" id="openListCategory">
                        <span style="font-size: 12px;color: #fff;font-weight: bold;">
                            DANH MỤC
                        </span>
                            </button>

                        </div>
                        <div class="item align-items-center">
                            <div class="form-group">
                                <form method="GET" action="http://caygiong123.com/tim-kiem">
                                    <input
                                        value=""
                                        name="q"
                                        type="text"
                                        placeholder="Tìm kiếm cây giống ..."
                                        class="form-control form-control-success">
                                </form>
                            </div>
                        </div>
                        <div class="item" onclick="openNav()">
                            <div class="action-menu d-flex flex-column justify-content-between align-items-center">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                                <span>
                            MENU
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <main>
        <div class="text-run-top d-flex align-items-center">
            <div class="container abc">
                <div class="row">
                    <div class="col-md-12">
                        <marquee>
                    <span class="font-weight-bold">

                                                 Chào mừng bạn đến với Trung tâm giống cây trồng Học Viện Nông Nghiệp. Hãy liên hệ với chúng tôi để được tư vấn: 0394.253.666
                    </span>
                        </marquee>
                    </div>
                </div>
            </div>
        </div>
        <div class="container abc">
            <div class="row">
                <div class="col-md-3">
                    <div class="menu-master" id="myDIVPro" style="display: none;">
                        <div class="d-flex flex-column">
                            <a href="index.html">Trang chủ</a>
                            <a href="gioi-thieu.html">Giới thiệu</a>
                            <a href="thanh-toan.html">Hướng dẫn mua hàng</a>
                            <a href="danh-sach-tin-tuc.html">Tin tức thị trường</a>
                            <a href="lien-he.html">Liên hệ</a>
                            <a href="gio-hang.html">
                                Giỏ hàng (<span id="data-Cart">0</span>)
                            </a>

                        </div>
                    </div>
                    <ul class="nav flex-column side-left" id="myDIV" style="display: none;">
                        <li class="nav-item branch">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <span>Danh mục sản phẩm</span>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="17-cay-buoi-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Bưởi
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="18-cay-cam-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Cam
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="19-cay-chanh-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Chanh
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="20-cay-chuoi-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Chuối
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="21-cay-du-du-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Đu Dủ
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="22-cay-hong-xiem-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Hồng Xiêm Xoài
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="24-cay-mit-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống Cây Mít
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="25-cay-nhan-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống Cây Nhãn
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="26-cay-nho-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Nho
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="27-cay-oi-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Ổi
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="28-cay-sau-rieng-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Cây sầu riêng giống
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="29-cay-tao-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Cây Táo Giống
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="30-cay-tram-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Trám
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="31-cay-xoai-giong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Xoài
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="45-cay-lat.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Cây Lát
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="65-cay-giong-quit.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Quýt
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="66-cay-giong-phat-thu.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Cây Giống Phật Thủ
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="67-cay-giong-coc.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Cóc
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="68-cay-giong-vai.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giông Cây Vải
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="69-cay-giong-mang-cut.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Măng Cụt
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="70-cay-giong-dinh-lang.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                CÂY GIỐNG ĐINH LĂNG
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="71-cay-giong-bo.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống Cây  Bơ
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="72-cay-giong-dua.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Dừa
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="73-cay-giong-cau.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Cau
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="74-cay-giong-thanh-long.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Thanh Long
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="75-cay-giong-hong.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Hồng Giòn
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="76-cac-loai-giong-khac.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Các Loại Cây Giống Khác
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="77-cay-giong-na.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Na
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="78-cay-giong-sung-my.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                Giống cây Sung Mỹ
                                            </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="79-cay-giong-long-lao.html">

                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                                                CÂY CÔNG TRÌNH
                                            </span>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav flex-column side-left d-none d-sm-block">
                        <li class="nav-item branch">

                            <span>Danh mục sản phẩm</span>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="17-cay-buoi-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Bưởi
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="18-cay-cam-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Cam
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="19-cay-chanh-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Chanh
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="20-cay-chuoi-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Chuối
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="21-cay-du-du-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Đu Dủ
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="22-cay-hong-xiem-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Hồng Xiêm Xoài
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="24-cay-mit-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống Cây Mít
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="25-cay-nhan-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống Cây Nhãn
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="26-cay-nho-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Nho
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="27-cay-oi-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Ổi
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="28-cay-sau-rieng-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Cây sầu riêng giống
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="29-cay-tao-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Cây Táo Giống
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="30-cay-tram-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Trám
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="31-cay-xoai-giong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Xoài
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="45-cay-lat.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Cây Lát
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="65-cay-giong-quit.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Quýt
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="66-cay-giong-phat-thu.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Cây Giống Phật Thủ
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="67-cay-giong-coc.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Cóc
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="68-cay-giong-vai.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giông Cây Vải
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="69-cay-giong-mang-cut.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Măng Cụt
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="70-cay-giong-dinh-lang.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    CÂY GIỐNG ĐINH LĂNG
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="71-cay-giong-bo.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống Cây  Bơ
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="72-cay-giong-dua.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Dừa
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="73-cay-giong-cau.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Cau
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="74-cay-giong-thanh-long.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Thanh Long
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="75-cay-giong-hong.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Hồng Giòn
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="76-cac-loai-giong-khac.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Các Loại Cây Giống Khác
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="77-cay-giong-na.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Na
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="78-cay-giong-sung-my.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    Giống cây Sung Mỹ
                </span>
                            </a>
                        </li>
                        <li class="nav-item category-item">
                            <a class="nav-link" href="79-cay-giong-long-lao.html">


                                <img src="images/web/Logo/logo.png" alt=""
                                     style="width: 18px; height: 18px; margin-right: 10px;">
                                <span>
                    CÂY CÔNG TRÌNH
                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 master-right-content">
                    <div class="banner-list d-none">
                        <div class="owl-carousel owl-theme">
                            <a target="_blank" href="#" alt="">
                                <div class="banner-item">
                                    <img class="owl-lazy lazyOwl"
                                         data-src="../api.caygiong123.com/storage/images/banners/2021_03_30_e1f59dae835e33dcbb3446a35faf93f3.jpg">
                                </div>
                            </a>
                            <a target="_blank" href="#" alt="">
                                <div class="banner-item">
                                    <img class="owl-lazy lazyOwl"
                                         data-src="../api.caygiong123.com/storage/images/banners/2021_03_30_a9f58c55a8314b7131f9ced1c11fc7e2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="group-title">
            <span class="item">
                Cây giống nổi bật
            </span>
                    </div>
                    <div class="seedling-highlight">
                        <div class="owl-carousel">
                            <div class="seedling-highlight-item">
                                <a href="cay-giong/27-nhan-long-hung-yen.html">
                                    <div class="seedling-highlight-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_b280d60f9ce24a886428b01ecc546c7f.jpg);">

                                    </div>
                                </a>
                                <div
                                    class="seedling-highlight-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">NHÃN LỒNG HƯNG YÊN</span>
                                    </div>
                                    <div>
                                        <span class="price">30.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/27-nhan-long-hung-yen.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/27-nhan-long-hung-yen.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-highlight-item">
                                <a href="cay-giong/28-nho-than-go-tu-quy.html">
                                    <div class="seedling-highlight-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_4199e3fb319599e9f4e6e0cb5d71eff3.jpg);">

                                    </div>
                                </a>
                                <div
                                    class="seedling-highlight-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">NHO THÂN GỖ TỨ QUÝ</span>
                                    </div>
                                    <div>
                                        <span class="price">120.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/28-nho-than-go-tu-quy.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/28-nho-than-go-tu-quy.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-highlight-item">
                                <a href="cay-giong/37-mang-cut.html">
                                    <div class="seedling-highlight-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_875bd79198b412ca2d6da00b688113d5.jpg);">

                                    </div>
                                </a>
                                <div
                                    class="seedling-highlight-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Măng cụt</span>
                                    </div>
                                    <div>
                                        <span class="price">100.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/37-mang-cut.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/37-mang-cut.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-highlight-item">
                                <a href="cay-giong/40-thanh-long-ruot-do.html">
                                    <div class="seedling-highlight-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_980437a2eafa006e1af1bc46a2d91261.jpg);">

                                    </div>
                                </a>
                                <div
                                    class="seedling-highlight-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">THANH LONG RUỘT ĐỎ</span>
                                    </div>
                                    <div>
                                        <span class="price">15.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/40-thanh-long-ruot-do.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/40-thanh-long-ruot-do.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="group-title">
        <span class="item">
            Tất cả cây giống
        </span>
                    </div>
                    <div class="all-seedlings">
                        <div class="row">
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/70-cay-mit-thai-choai-cay-to.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_14_653629f107e56e16ca71a18db400193c.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name"> Cây Mít Thái Choai...</span>

                                    </div>
                                    <div>
                                        <span class="price">250.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/70-cay-mit-thai-choai-cay-to.html"
                                       class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/70-cay-mit-thai-choai-cay-to.html"
                                       class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/69-tao-dao-vang.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_02a7b74312da3578168f5d16be3ccaee.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Táo Đào Vàng</span>

                                    </div>
                                    <div>
                                        <span class="price">20.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/69-tao-dao-vang.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/69-tao-dao-vang.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/68-tao-le-dai-loan.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_f278679ae44b4acde7cf85b3cbb145ff.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Táo lê đài loan</span>

                                    </div>
                                    <div>
                                        <span class="price">20.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/68-tao-le-dai-loan.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/68-tao-le-dai-loan.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/67-chuoi-tieu-hong.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_ab3eb92bee5e3f123beb350a05898a3f.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">CHUỐI TIÊU HỒNG</span>

                                    </div>
                                    <div>
                                        <span class="price">10.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/67-chuoi-tieu-hong.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/67-chuoi-tieu-hong.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/66-nho-tim-ninh-thuan.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_172f55a70c6e3ee7834881e29f6fd59e.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Nho tím ninh thuận</span>

                                    </div>
                                    <div>
                                        <span class="price">100.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/66-nho-tim-ninh-thuan.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/66-nho-tim-ninh-thuan.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/65-xoai-dai-loan.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_645c8560adadbeef39aeb456f9eb3d70.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">XOÀI ĐÀI LOAN</span>

                                    </div>
                                    <div>
                                        <span class="price">40.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/65-xoai-dai-loan.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/65-xoai-dai-loan.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/64-xoai-thai-xanh.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_14002954783e920c5ecdcebbb5c83a49.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">XOÀI THÁI XANH</span>

                                    </div>
                                    <div>
                                        <span class="price">40.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/64-xoai-thai-xanh.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/64-xoai-thai-xanh.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/63-khe-ngot.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_8f41f19000f0f4a280bfcb7c9a79d2bc.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">KHẾ NGỌT</span>

                                    </div>
                                    <div>
                                        <span class="price">25.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/63-khe-ngot.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/63-khe-ngot.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/62-chuoi-tay-thai.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_2e89d2046f214d25c5c84ea605daf21b.png);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Chuối Tây Thái</span>

                                    </div>
                                    <div>
                                        <span class="price">13.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/62-chuoi-tay-thai.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/62-chuoi-tay-thai.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/61-sa-ke.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_1ffad94f4f114ccd652b6e1a5c6c87d6.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Sa kê</span>

                                    </div>
                                    <div>
                                        <span class="price">120.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/61-sa-ke.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/61-sa-ke.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/60-na-dai.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_009b9f13617a83d4de453f6e94f761cd.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">Na dai</span>

                                    </div>
                                    <div>
                                        <span class="price">8.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/60-na-dai.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/60-na-dai.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/59-vu-sua-tim.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_e61bcc292d743b483daaab6c71d35011.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">VÚ SỮA TÍM</span>

                                    </div>
                                    <div>
                                        <span class="price">45.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/59-vu-sua-tim.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/59-vu-sua-tim.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/58-oi-le-dai-loan.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_d957668fa276fcaf6f141112a4964e43.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">ỔI LÊ ĐÀI LOAN</span>

                                    </div>
                                    <div>
                                        <span class="price">25.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/58-oi-le-dai-loan.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/58-oi-le-dai-loan.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/57-le-nau-mac-cop.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_9b40b804ded82592f427941fb15b8c05.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">LÊ NÂU ( MẮC CỌP)</span>

                                    </div>
                                    <div>
                                        <span class="price">35.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/57-le-nau-mac-cop.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/57-le-nau-mac-cop.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/56-tao-do-lun-f1.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_5261581395c5b9adef7952da02dafa74.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">TÁO ĐỎ LÙN F1</span>

                                    </div>
                                    <div>
                                        <span class="price">80.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/56-tao-do-lun-f1.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/56-tao-do-lun-f1.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="seedling-item pading-0">
                                <a href="cay-giong/55-bon-bon.html">
                                    <div class="seedling-image"
                                         style="background-image: url(../api.caygiong123.com/storage/images/seedlings/sm_2021_04_02_e33ab3f7b3f640754570245cafa87d58.jpg);">

                                    </div>
                                </a>
                                <div class="seedling-info d-flex flex-column flex-lg-row justify-content-between">
                                    <div class="d-flex flex-column">
                                        <span class="name">BÒN BON</span>

                                    </div>
                                    <div>
                                        <span class="price">50.000</span>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mt-2 seedling-highlight-control">
                                    <a href="cay-giong/55-bon-bon.html" class="btn btn-success btn-sm">
                                        Đặt hàng
                                    </a>
                                    <a href="cay-giong/55-bon-bon.html" class="btn btn-success btn-sm">
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="seedling-panigation">
                            <ul class="pagination">

                                <li class="page-item disabled"><span class="page-link">&laquo;</span></li>


                                <li class="page-item active"><span class="page-link">1</span></li>
                                <li class="page-item"><a class="page-link" href="index4658.html?page=2">2</a></li>
                                <li class="page-item"><a class="page-link" href="index9ba9.html?page=3">3</a></li>
                                <li class="page-item"><a class="page-link" href="indexfdb0.html?page=4">4</a></li>


                                <li class="page-item"><a class="page-link" href="index4658.html?page=2" rel="next">&raquo;</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="tu-van">
        <div class="container">
            <div class="tu-van-wrap d-flex justify-content-between align-items-center">
                <div class="d-flex flex-column">
                    <strong>ĐĂNG KÝ TƯ VẤN</strong>
                    <span>Gửi Zalo cho chúng tôi để nhận thông tin về sản phẩm phù hợp dành cho bạn</span>
                </div>
                <div>
                    <a target="_blank" href="https://zalo.me/0394253666" class="btn btn-success">
                        ĐĂNG KÍ NGAY
                    </a>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="big-footer big-footer123 footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="name-center">Trung Tâm Giống Cây Trồng Học Viện Nông Nghiệp</p>
                        <p class="mb-2">
                            - Với phương châm uy tín và chất lượng đi hàng đầu Trung tâm cây giống Trường Đại Học Nông
                            Nghiệp 1 Hà Nội trong bao năm qua luôn tự hào đã mang tới cho các bạn những cây giống tốt
                            nhất, giá phù hợp nhất và đem lại hiệu quả về mặt kinh tế cao nhất
                        </p>
                        <p>- Quý khách có nhu cầu tư vấn báo giá sản phẩm, vui lòng liên hệ cho chúng tôi tại: </p>
                        <p>
                            <i class="fa fa-map-marker-alt"></i>
                            <span class="font-weight-bold" style="color: #29487d">Địa chỉ: Học Viện Nông Nghiệp Việt Nam - Trâu Quỳ - Gia Lâm - Hà Nội</span>
                        </p>
                        <p>
                            <i class="fa fa-mobile-alt"></i>
                            <a href="tel:0363525113"><strong>Số điện thoại: <span style="color: red">0394.253.666</span></strong></a>
                        </p>
                        <p>
                            <i class="far fa-envelope" style="padding-right: 5px;"></i>
                            Email: dangcaygiong123@gmail.com
                        </p>
                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-4 facebook">
                        <div class="fb-page"
                             data-href="https://www.facebook.com/Trung-T%C3%A2m-Gi%E1%BB%91ng-C%C3%A2y-Tr%E1%BB%93ng-Vi%E1%BB%87n-N%C3%B4ng-Nghi%E1%BB%87p-101566751248410/"
                             data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                             data-show-facepile="true">
                            <blockquote
                                cite="https://www.facebook.com/Trung-T%C3%A2m-Gi%E1%BB%91ng-C%C3%A2y-Tr%E1%BB%93ng-Vi%E1%BB%87n-N%C3%B4ng-Nghi%E1%BB%87p-101566751248410//"
                                class="fb-xfbml-parse-ignore"><a
                                    href="https://www.facebook.com/Trung-Tâm-Giống-Cây-Trồng-Viện-Nông-Nghiệp-101566751248410/">Trung
                                    Tâm Giống Cây Trồng - Viện Nông Nghiệp</a></blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mini-footer d-flex align-items-center justify-content-center">
            <div class="container">
                <div class="col-md-12 v424-up">
                    <div class="d-flex justify-content-center">
                        <span>Bản quyền 2021 © Cây giống 123 | Cây giống tốt nhất</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div id="myBtnCall">
    <a href="tel:0394253666">
                <span class="active_phone2">
                    <i class="fa fa-phone"></i>
                    <span class="cover">
                        <span class='wave2 one'></span>
                        <span class='wave2 two'></span>
                        <span class='wave2 three'></span>
                    </span>
                </span>
    </a>
</div>

<script type="text/javascript" src="{!! asset('js/jquery-2.1.4.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>


<!-- Global site tag (gtag.js) - Google Analytics -->

<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-193420462-1');
</script>
<script type="text/javascript">
    /* Set the width of the side navigation to 250px */
    function openNav() {
        // document.getElementById("mySidenav").style.width = "250px";
        $('#myDIVPro').slideToggle();
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    $('#openListCategory').click(function () {
        $('#myDIV').fadeToggle();
    });

</script>
<script type="text/javascript" src="{!! asset('web/librarys/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') !!}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // Ảnh các hãng Banner
        $(".banner-list .owl-carousel").owlCarousel({
            loop: false,
            dots: false,
            nav: false,
            responsiveClass: true,
            autoplay: true,
            autoplayTimeout: 4500,
            autoplaySpeed: 1200,
            autoplayHoverPause: true,
            lazyLoad: true,
            lazyLoadEager: 0,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots: false,
                    loop: true,
                },
                768: {
                    items: 1,
                    nav: true,
                    dots: true,
                    loop: true
                }
            }
        });
        $('.owl-prev').text('');
        $('.owl-next').text('');

        // Carousel phần banner-top trang chủ Home
        $(".seedling-highlight .owl-carousel").owlCarousel({
            loop: true,
            dots: false,
            responsiveClass: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 1000,
            responsive: {
                0: {
                    items: 2,
                    nav: false,
                    loop: true,
                },
                768: {
                    items: 4,
                    nav: false,
                    loop: true,
                }
            }
        });
    });
</script>
</body>

<!-- Mirrored from caygiong123.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Apr 2021 05:46:38 GMT -->
</html>
